package org.webswing.model.s2c;

import java.io.Serializable;

public class JsonWindowPartialContent implements Serializable {

    private static final long serialVersionUID = -7577833300925947305L;

    Integer positionX;
    Integer positionY;
    Integer width;
    Integer height;
    String base64Content;

    public Integer getPositionX() {
        return positionX;
    }

    public void setPositionX(Integer positionX) {
        this.positionX = positionX;
    }

    public Integer getPositionY() {
        return positionY;
    }

    public void setPositionY(Integer positionY) {
        this.positionY = positionY;
    }

    public String getBase64Content() {
        return base64Content;
    }

    public void setBase64Content(String base64Content) {
        this.base64Content = base64Content;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "JsonWindowPartialContent [positionX=" + positionX + ", positionY=" + positionY + ", width=" + width + ", height=" + height + "]";
    }

}
